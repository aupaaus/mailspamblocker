local f = CreateFrame("Frame")

f:RegisterEvent("MAIL_SHOW");
f:SetScript("OnEvent", function(self, event, ...)
	CheckInbox();
	numitems, totalitems = GetInboxNumItems();
	-- check if inbox downloaded first
	if numitems ~= 0 then
		ntotalitems = totalitems
		for i=1, totalitems do
			if i == 1 then
				startpoint = totalitems 
			else
				startpoint = startpoint - 1
			end

			pckicon, staticon, sender, subject, money, CODAmount, daysLeft, hasItem, textCreated, canReply, isGM = GetInboxHeaderInfo(startpoint);
			inmail, intexture, istake, isinvo = GetInboxText(startpoint);
			if inmail ~= nil then
	 			if string.match(inmail, "discount") or string.match(subject, "discount") then 
					if money == 0 then
						if hasItem == nil then
	 						print('deleting spam at ' .. startpoint);
							DeleteInboxItem(startpoint)
						end
					end
	 			end
			end
		end
	end

end)

